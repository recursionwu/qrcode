#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

#define DATA_DIR_PATH    "./DATA"
#define ERROR_FILE_PATH  "./error.txt"
#define OUTPUT_REPEAT_PATH "./repeat.txt"
#define OUTPUT_NO_REPEAT_PATH "./no_repeat.txt"
#define FILE_NAME_LEN    1024
#define QR_INFO_LEN      1024
#define ERROR_STR_LEN    256
#define OUTPUT_LINE_LEN  1024

struct file_line_num_node
{
    struct file_line_num_node *next;;
    char file_name[FILE_NAME_LEN];
    long long int line_num;
};

struct qr_node
{
    struct qr_node *next;
    char   qr_info[QR_INFO_LEN];
    int    repeat;
    struct file_line_num_node  file_line_num_list_head;
};

struct qr_node g_qr_list_head;

static FILE *p_error_f     = NULL;
static FILE *p_repeat_f    = NULL;
static FILE *p_no_repeat_f = NULL;


static void LOGE(char *error_string)
{
    char str[ERROR_STR_LEN] = {0};

    if (!error_string) {
        LOGE("NULL poiter enter LOGE\n");
    }

    if (!p_error_f) {
        p_error_f = fopen(ERROR_FILE_PATH, "w+");
        if (!p_error_f) {
            printf("open ./error.log failed!");
            exit(0);
        }
    }
    sprintf(str, "%s\n", error_string);

    fputs(str, p_error_f);
}

static void __output(void)
{
    struct qr_node     *p_qr_node = NULL;

    struct file_line_num_node  *p_file_line_num_node = NULL;

    long long int    no_repeat_count  = 0;

    char   output_line[OUTPUT_LINE_LEN] = {0};


    p_no_repeat_f = fopen(OUTPUT_NO_REPEAT_PATH, "w+");
    p_repeat_f    = fopen(OUTPUT_REPEAT_PATH, "w+");

    if (!p_no_repeat_f || !p_repeat_f) {
        LOGE("output file create failed");
        return;
    }

    //hlist_for_each_entry_safe(p_qr_node_tpos, p_qr_node_pos, p_qr_node_n, &g_qr_list, h_node)
    // list_for_each(p_qr_node, &g_qr_list_head)
    for (p_qr_node = g_qr_list_head.next; p_qr_node; p_qr_node = p_qr_node->next)
    {
        no_repeat_count++;

        sprintf(output_line, "%s\n", p_qr_node->qr_info);
        fputs(output_line, p_no_repeat_f);

        if (p_qr_node->repeat)
        {
            long long int repeat_count = 0;
            sprintf(output_line, "%s repeat:\n", p_qr_node->qr_info);
            fputs(output_line, p_repeat_f);

            //            list_for_each(p_file_line_num_node, &(p_qr_node_tpos->file_line_num_list_head))
            for (p_file_line_num_node = (p_qr_node->file_line_num_list_head).next; p_file_line_num_node; p_file_line_num_node = p_file_line_num_node->next)
            {
                repeat_count++;

                sprintf(output_line, "%lld appear in file %s, line %lld\n",
                        repeat_count,
                        p_file_line_num_node->file_name,
                        p_file_line_num_node->line_num);

                fputs(output_line, p_repeat_f);
            }

            fputs("\n", p_repeat_f);
        }
    }
}

static void __add_file_line_num_node(struct file_line_num_node *p_file_line_num_list_head, char *file_name, long long line_num)
{
    struct file_line_num_node *p_file_line_num_node = NULL;

    if (NULL == (p_file_line_num_node = (struct file_line_num_node *)malloc(sizeof(struct file_line_num_node)))) {
        LOGE("malloc failed");
        return;
    }

    memset(p_file_line_num_node, 0, sizeof(struct file_line_num_node));
    strncpy(p_file_line_num_node->file_name, file_name, FILE_NAME_LEN - 1);
    p_file_line_num_node->line_num = line_num;

    p_file_line_num_node->next      = p_file_line_num_list_head->next;
    p_file_line_num_list_head->next = p_file_line_num_node;
}

static void __add_repeat_qr_node(struct qr_node *p_qr_node, char *file_name, long long int line_num)
{
    p_qr_node->repeat = 1;

    __add_file_line_num_node(&(p_qr_node->file_line_num_list_head), file_name, line_num);
}



static void __add_new_qr_node(char *qr_info, char *file_name, long long int line_num)
{
    struct qr_node *p_qr_node = NULL;

    if (NULL == (p_qr_node = (struct qr_node *)malloc(sizeof(struct qr_node)))) {
        LOGE("malloc failed");
        return;
    }
    else {
        memset(p_qr_node, 0, sizeof(struct qr_node));
        strncpy(p_qr_node->qr_info, qr_info, QR_INFO_LEN - 1);
        __add_file_line_num_node(&(p_qr_node->file_line_num_list_head), file_name, line_num);
    }

    p_qr_node->next = g_qr_list_head.next;
    g_qr_list_head.next = p_qr_node;
}

static void __qr_entry_check(char *line, char *file_name, long long int line_num)
{
    struct qr_node  *p_qr_node_tmp = NULL;

    char *p_http = NULL;
    char* sepra  = NULL;

    int found = 0;
    char error_str[ERROR_STR_LEN] = {0};

    if (!strlen(line)) {
        return;
    }

    if ( NULL == (p_http = strstr(line , "http"))) {
        return;
    }

    sepra = strchr(p_http, ',');

    if (sepra) {
        *sepra = '\0';
    }

    sepra = strchr(p_http, ' ');

    if (sepra) {
        *sepra = '\0';
    }

    sepra = strchr(p_http, '\r');

    if (sepra) {
        *sepra = '\0';
    }

    for (p_qr_node_tmp = g_qr_list_head.next; p_qr_node_tmp; p_qr_node_tmp = p_qr_node_tmp->next)
    {
        if (0 == strcmp(line, p_qr_node_tmp->qr_info))
        {
            found = 1;
            __add_repeat_qr_node(p_qr_node_tmp, file_name, line_num);
            /*TBD*/
        }
    }

    if (!found) {
        __add_new_qr_node(p_http, file_name, line_num);
    }

}

static void __file_check(char *file_name)
{
    FILE* f = NULL;
    char error_str[ERROR_STR_LEN] = {0};
    char *sepra = NULL;

    char line[256] = {0};

    if ( !(f = fopen(file_name, "r"))) {
        sprintf(error_str, "%s open failed.", file_name);
        LOGE(error_str);
    }

    else {
        long long int line_num = 0;

        while ( fgets(line, sizeof(line), f)) {

            line_num++;

            __qr_entry_check(line, file_name, line_num);

        }
    }
}

int main (void)
{
    DIR *dp = NULL;
    char file_name[1024] = {0};
    struct dirent *dirp = NULL;
    FILE *f = NULL;
    char error_str[128] = {0};

    if ( NULL == (dp = opendir(DATA_DIR_PATH))) {
        printf("DATA directroy not found!\n");
        LOGE("DATA directory not found!");
        exit(0);
    }

    while ( NULL != (dirp = readdir(dp))) {

        if ((0 == strcmp(".", dirp->d_name))
            || ( 0 == strcmp("..", dirp->d_name)))
        {
            continue;
        }

        sprintf(file_name, "%s/%s", DATA_DIR_PATH, dirp->d_name);

        if ( NULL == (f = fopen(file_name, "r"))) {
            sprintf(error_str, "%s open failed!", file_name);
            LOGE(error_str);
            continue;
        }

        __file_check(file_name);
    }

    __output();
}
